-- This program tests the conjecture that all for all n, the corresponding n-tina
-- is homogeneous if and only if 2*n + 1 is an odd prime.
module Main where

import Control.Monad
import Control.Monad.Except
import Control.Monad.ST

import Data.Array.ST
import Data.List (genericLength, intercalate, nub)

import System.IO

data Counterexample = NotPrimeHomogeneous Integer | PrimeNotHomogeneous Integer

instance Show Counterexample where
  show (NotPrimeHomogeneous n) = show n ++ " violates conjecture: homogeneous but not prime!"
  show (PrimeNotHomogeneous n) = show n ++ " violates conjecture: prime but not homogeneous!"

-- Lazily evaluated list of all odd primes
oddPrimes :: [Integer]
oddPrimes = 3 : filter isPrime [5,7..]
  where isPrime x = all ((/=0) . mod x) $ takeWhile ((<=x) . (^2)) oddPrimes

-- The n-tina permutation is also known as the twist permutation
twist :: Integer -> Integer -> Integer
twist n k
  | 2*k <= n = 2*k
  | 2*k >  n = 2*(n - k) + 1

cycleLength :: Integer -> Integer -> Integer
cycleLength n start = (1+) . genericLength . takeWhile (/=start) . tail $ iterate (twist n) start

-- This function takes an `n` and returns the list of cycles found in the corresponding `n`-tina
findCycles :: Integer -> [Integer]
findCycles n = runST $ newArray (1,n) False >>= findRemainingCycles n
  where
    -- Finds the remaining cycles using
    -- The first argument keeps track of how many positions are left to visit
    -- The second argument keeps track of which positions have been visited
    findRemainingCycles :: Integer -> STUArray s Integer Bool -> ST s [Integer]
    findRemainingCycles 0 _             = return []
    findRemainingCycle numLeft visited = do
      nextCycleStart <- findFirstUnvisited 1 visited
      let nextCycleLength = cycleLength n nextCycleStart
      visitTwists nextCycleLength nextCycleStart visited
      remainingCycles <- findRemainingCycles (numLeft - nextCycleLength) visited
      return $ nextCycleLength:remainingCycles

    -- From given start position in the second argument, applies the twist
    -- permutation a number of times equal to the first argument and sets
    -- each encountered position to visited in the modifiable array given
    -- as the third argument
    visitTwists :: Integer -> Integer -> STUArray s Integer Bool -> ST s ()
    visitTwists 0 _ _                 = return ()
    visitTwists numLeft start visited = do
      writeArray visited start True
      visitTwists (numLeft - 1) (twist n start) visited

    findFirstUnvisited :: Integer -> STUArray s Integer Bool -> ST s Integer
    findFirstUnvisited start visited = do
      isVisited <- readArray visited start
      if isVisited
        then findFirstUnvisited (start+1) visited
        else return start

-- Returns true if all elements of the list are the same
-- Because of laziness it will immediately return false if a second value is found
homogeneous :: [Integer] -> Bool
homogeneous cycles
  | [_] <- nub cycles = True
  | otherwise         = False

-- Formats cycles for output and outputs them to the given handle
hPutCycles :: Handle -> Integer -> [Integer] -> IO ()
hPutCycles handle n cycles = do
  hPutStrLn handle $ intercalate "," $ fmap show $ n:cycles
  hFlush handle

main :: IO ()
main = withFile "out.csv" WriteMode $
  \handle ->
    either (hPrint handle) (const $ return ()) =<<
      runExceptT (
        forM_ (zip oddPrimes $ tail oddPrimes) $
          \(prime, nextPrime) -> do
            let
              firstN = (prime - 1) `div` 2
              lastN  = (nextPrime - 1) `div` 2 - 1
              primeCycles = findCycles firstN
            liftIO $ hPutCycles handle firstN primeCycles
            -- check that all odd primes correspond to homogenenous n-tinas
            unless (homogeneous primeCycles) $ throwError $ PrimeNotHomogeneous firstN
            forM_ [firstN+1..lastN] $
              \n -> do
                let cycles = findCycles n
                liftIO $ hPutCycles handle n cycles
                -- check that all homogenenous n-tinas correspond to odd primes
                when (homogeneous cycles) $ throwError $ NotPrimeHomogeneous n
      )
