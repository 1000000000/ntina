# *n*-tina

This program tests the conjecture that the sequence of homogeneous *n*-tinas is the same as the sequence of odd primes minus one divided by two.

Output is placed into the `out.csv`.
The program will run until it finds a counterexample to the conjecture.

### Homogeneous *n*-tinas

*n*-tinas are a generalization of one of the ideas of the sestina poetic form.
The sestina has a pattern of limited line ending words whose order in each stanza is permuted by the twist permutation defined generally for permuting *n* things below.

    T_n(k)
     | k ≤ n/2 = 2*k
     | k > n/2 = 2*(n - k) + 1

I call a *n*-tina homogeneous if all the cycles in the cycle decomposition of the permutation T\_n have the same order.
